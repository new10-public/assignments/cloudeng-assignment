# Devops Exercise for New10

Under the `infra` folder you will find a Terraform module that creates an ECS service than runs a simple nginx website, and a deployment to eu-west-1 region of that module.

We would like you to turn this simple module into an internet facing service with an active-active multi-region deployment.

Some more hints (to make things better):
* Feel free to use Terraform, TypeScript, Python 
* Think about what you think we want to achieve with:
  * an active-active setup, 
  * what other capabilities we need
  * what could go wrong and how to prevent / cope with this
  * and design your solution around these things!
* Document your thoughts!

Bonus points:
* Add a CICD pipeline for this
