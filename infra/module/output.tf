# # ECS
# output "ecs_cluster_id" {
#   value = "${aws_ecs_cluster.cluster.id}"
# }

# output "ecs_cluster_name" {
#   value = "${aws_ecs_cluster.cluster.name}"
# }

# # SG
# output "ecs_cluster_sg_id" {
#   value = "${aws_security_group.ecs_security_group.id}"
# }

# output "ecs_cluster_sg_name" {
#   value = "${aws_security_group.ecs_security_group.name}"
# }

# # ASG
# output "ecs_cluster_asg_id" {
#   value = "${aws_autoscaling_group.ecs.id}"
# }

# output "ecs_cluster_asg_name" {
#   value = "${aws_autoscaling_group.ecs.name}"
# }

# output "ecs_cluster_asg_arn" {
#   value = "${aws_autoscaling_group.ecs.arn}"
# }

# # IAM
# output "ecs_cluster_role_arn" {
#   value = "${aws_iam_role.cluster_role.arn}"
# }
